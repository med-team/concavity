// Source file for PDBMolecule structure



// Include files

#include "PDB.h"



PDBMolecule::
PDBMolecule(const char *name)
  : bbox(R3null_box)
{
  // Assign ID
  static int PDBnext_molecule_id = 0;
  id = PDBnext_molecule_id++;

  // Copy name
  if (name) strncpy(this->name, name, 32);
  else this->name[0] = '\0';
}



PDBChain *PDBMolecule::
FindChain(const char *name) const
{
  // Check name
  if (!name) return NULL;

  // Find chain 
  for (int i = 0; i < NChains(); i++) {
    PDBChain *chain = Chain(i);
    if (!strcmp(chain->Name(), name)) 
      return chain;
  }
  return NULL;
}



PDBResidue *PDBMolecule::
FindResidue(PDBChain *chain, const char *name, int sequence, int insertion_code) const
{
  // Search chain, if possible
  if (chain) return chain->FindResidue(name, sequence, insertion_code);

  // Otherwise, find residue among others
  for (int i = 0; i < NResidues(); i++) {
    PDBResidue *residue = Residue(i);
    if (residue->Chain()) continue;
    if (sequence != residue->Sequence()) continue;
    if (insertion_code != residue->InsertionCode()) continue;
    if (name) { if ((!residue->Name()) || (strcmp(name, residue->Name()))) continue; }
    else if (residue->Name()) continue;
    return residue;
  }
  return NULL;
}



PDBResidue *PDBMolecule::
FindResidue(const char *str) const
{
  // Copy string (strtok destroys it)
  char buffer[1024];
  strncpy(buffer, str, 1024);

  // Parse PDB code
  char *bufferp = strtok(buffer, "/");
  if (!bufferp) { 
    fprintf(stderr, "PDB code error in residue expression: %s\n", str); 
    return 0; 
  }
  while (isspace(*bufferp)) bufferp++;
  char *pdb_code = strdup(bufferp);

  // Parse model identifier
  bufferp = strtok(NULL, "/");
  if (!bufferp) { 
    fprintf(stderr, "Model ID error in residue expression: %s\n", str); 
    return 0; 
  }
  while (isspace(*bufferp)) bufferp++;
  int model_id = atoi(bufferp);
    
  // Parse chain identifier
  bufferp = strtok(NULL, "/");
  if (!bufferp) { 
    fprintf(stderr, "Chain ID error in residue expression: %s\n", str); 
    return 0; 
  }
  char *chain_name = strdup(bufferp);
    
  // Parse residue name
  bufferp = strtok(NULL, ".");
  if (!bufferp) { 
    fprintf(stderr, "Residue name error in residue expression: %s\n", str); 
    return 0; 
  }
  while (isspace(*bufferp)) bufferp++;
  char *residue_name = strdup(bufferp);

  // Parse residue sequence number
  bufferp = strtok(NULL, "./");
  if (!bufferp) { 
    fprintf(stderr, "Residue sequence error in residue expression: %s\n", str); 
    return 0; 
  }
  while (isspace(*bufferp)) bufferp++;
  int residue_sequence = atoi(bufferp);

  // Parse residue insertion code
  int residue_insertion_code = ' ';
  bufferp = strtok(NULL, "./");
  if (bufferp) {  
    while (isspace(*bufferp)) bufferp++; 
    residue_insertion_code = *bufferp; 
  }

  // Find residue in molecule
  return FindResidue(chain_name, residue_name, residue_sequence, residue_insertion_code);
}
